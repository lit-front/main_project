import { LitElement, html, css } from 'lit-element';
import "./tareas-app"
class BodyApp extends LitElement {
  static get properties(){
    return{
    };
  } 
  constructor(){
    super();
  }
  static get styles(){
    return css`
      div{
        text-align:center;
      }
    `
  }
  render() { 
    return html`
      <div class='main'>
        <tareas-app title='Lista de tareas' tareas='["Empezar","Terminar",":}"]'>${this.title}</tareas-app>
      </div>
  `;
  } 
}  
customElements.define('body-app', BodyApp) 