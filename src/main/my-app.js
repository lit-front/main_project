import { LitElement, html } from 'lit-element';
import './header-app'
import './body-app'
import './footer-app'
class MyApp extends LitElement {
  static get properties(){
    return{

    };
  } 
  constructor(){
    super();
  }
  render() { 
    return html`
      <header-app></header-app>
      <body-app></body-app>
      <footer-app></footer-app>
  `;
  } 
}  
customElements.define('my-app', MyApp) 