import { LitElement, html, css } from 'lit-element';
class FooterApp extends LitElement {
  static get properties(){
    return{

    };
  } 
  constructor(){
    super();
  }
  static get styles(){
    return css`
      div{
        text-align: right;
        margin-top: 30%;
      }
    `
  }
  render() { 
    return html`
      <div class='footer'><h2>CM</h2></div>
  `;
  } 
}  
customElements.define('footer-app', FooterApp) 