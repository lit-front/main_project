import { LitElement, html, css } from 'lit-element';
class TareasApp extends LitElement {
  static get properties(){
    return{
      title: {type: String},
      tareas: {type: Array}
    };
  } 
  constructor(){
    super();
    this.title="Tareas";
    this.tareas=[];
  }
  static get styles(){
    return css`
      ul{
        list-style-type:none; padding:0px;
      }
      li{
        widght:100%;
        height:30px;
        line-height:30px;
        border-bottom:1px solid #ccc;
      }
      li:hover{
        background:#999; color:#fff;
      }
    `
  }
  render() { 
    return html`
      <center><h1>${this.title}</h1></center>
      <ul>
      ${this.tareas.map(
        function(tarea)
        {
          return html `
            <li>${tarea}</li>
          `;
        }
      )}
      </ul>
  `;
  } 
}  
customElements.define('tareas-app', TareasApp) 