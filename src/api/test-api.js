import { LitElement, html } from 'lit-element';
class TestApi extends LitElement {
  static get properties(){
    return{
      title: {type: String},
      movies: {type: Array}
    };
  } 
  constructor(){
    super();
    this.title="";
    this.movies=[];
    this.getMovieData();
  }

  render() {
    return html`
        <div>
            <center><h3>${this.title}</h3></center>
            <ul>
                ${this.movies.map(
                    function(movie){                    
                        return html `
                            <li>La pelicula ${movie.title}, fue dirigida por ${movie.director}</li>
                        `;
                    }
                )}
            </ul>
        </div>
    `;
  }

  getMovieData(){
    let xhr = new XMLHttpRequest();
    xhr.onload = function (){
        if (xhr.status == 200){
            let apiResponse = JSON.parse(xhr.responseText);
            this.movies = apiResponse.results;
            this.title = "Rest API";
        }
    }.bind(this);
    xhr.open("GET", "https://swapi.dev/api/films/");
    xhr.send();
  }

  getImage(){
    console.log("getImage");
    let xhr = new XMLHttpRequest();
    xhr.onload = function (){
        if (xhr.status == 200){
            console.log("Peticion completa correctamente");
            let apiResponse = JSON.parse(xhr.responseText);
            console.log("apiReponse: ", apiResponse);
            //this.movies = apiResponse.results;
            //console.log("movies: ",this.movies);
            
        }
    }.bind(this);
    xhr.open("GET", "https://dog.ceo/api/breeds/image/random");
    xhr.send();
  }


}  
customElements.define('test-api', TestApi) 