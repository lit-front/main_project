import { LitElement, html } from 'lit-element';

class ImgApi extends LitElement {
  static get properties(){
    return{
      img: {type:String},
      time: {type:Number}
    };
  } 
  constructor(){
    super();
    this.img = "";
    this.time = 0;
    this.getImage();
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="card">
        <img class="card-img-top" src="${this.img}" width='260%' height='480px'>
        <div class="card-body">
          <h5 class="card-title">Random</h5>
          <p class="card-text"></p>
          <p class="card-text"><small class="text-muted">Hace ${this.time} segundos</small></p>
        </div>
      </div>
    `;
  }

  getImage(){
    let xhr = new XMLHttpRequest();
    xhr.onload = function (){
        if (xhr.status == 200){
            let apiResponse = JSON.parse(xhr.responseText);
            this.img = apiResponse.message;           
        }
        setInterval(this.upTime.bind(this),1000);
    }.bind(this);
    xhr.open("GET", "https://dog.ceo/api/breeds/image/random");
    xhr.send();
  }

  upTime(){
    this.time ++;
  }

}  
customElements.define('img-api', ImgApi) 