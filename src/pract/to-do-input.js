import { LitElement, html, css } from 'lit-element';
class ToDoInput extends LitElement {
  /*
  static get properties(){
    return{
      prioridad: {type: Number},
      tarea: {type: String},
      id: {type: Number}
    };
  }
  */
  constructor(){
    super();
    this.prioridad=1;
    this.tarea="";
  }
  /*
  static get styles(){
    return css`
      ul{
        list-style-type:none; padding:0px;
      }
      li{
        widght:100%;
        height:30px;
        line-height:30px;
        border-bottom:1px solid #ccc;
      }
      li:hover{
        background:#999; color:#fff;
      }
    `
  }
  */
  render() {    
    return html`
      <div>
        <input type="text" value="${this.tarea}" @input="${this.updateTarea}" placeholder="Ingrese una tarea">
        <select @change="${this.updatePrioridad}">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
        </select>
        <input type="button" value="Agregar" @click="${this.agregarTarea}">
      </div>
    `;
  }

  updateTarea(e){
    this.tarea=e.target.value;
  }

  updatePrioridad(e){
    this.prioridad=e.target.value;
  }

  agregarTarea(){
    if (this.tarea.length > 3){
      this.dispatchEvent(new CustomEvent('agregar-tarea',{detail:{'texto':this.tarea,'prioridad':this.prioridad}}));
    }
  }

}  
customElements.define('to-do-input', ToDoInput) 