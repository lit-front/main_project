import { LitElement, html, css } from 'lit-element';
class ToDoTask extends LitElement {
  static get properties(){
    return{
      prioridad: {type: Number},
      tarea: {type: String},
      id: {type: Number}
    };
  } 
  constructor(){
    super();
    this.prioridad=0;
    this.tarea="";
    this.id=0;
  }
  static get styles(){
    return css`
      /*
      ul{
        list-style-type:none; padding:0px;
      }
      li{
        widght:100%;
        height:30px;
        line-height:30px;
        border-bottom:1px solid #ccc;
      }
      */
      li:hover{background:#999; color:#fff;}
      .card {display:inline-block;}
    `
  }
  render() {    
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="card border-info mb-3" style="mas-width: 18rem:">
      <div class="crad-header">Tarea #${this.id} - Prioridad ${this.prioridad}</div>
      <div class="card-body text-info">
      <h5 class="card-title">Tarea Pendiente</h5>
      <p class="card-text">${this.tarea}</p>
      <a href="#" class="btn btn-primary" @click="${this.removerTarea}">Eliminar</a>
      </div>
      </div>
    `;
  }

  removerTarea(){
    this.dispatchEvent(new CustomEvent('remover-tarea',{detail:{'id':this.id}}));
  }

}  
customElements.define('to-do-task', ToDoTask) 