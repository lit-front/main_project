import { LitElement, html, css } from 'lit-element';
import "./to-do-task"
import "./to-do-input"
class ToDoApp extends LitElement {
  static get properties(){
    return{
      title: {type: String},
      tareas: {type: Array},
    };
  } 
  constructor(){
    super();
    this.title="Ingreso de tareas";
    this.tareas=[];
  }
  static get styles(){
    return css`
      ul{
        list-style-type:none; padding:0px;
      }
      li{
        widght:100%;
        height:30px;
        line-height:30px;
        border-bottom:1px solid #ccc;
      }
      li:hover{
        background:#999; color:#fff;
      }
    `
  }
  render() {
    let ordenarTarea = this.tareas.sort((a,b)=>a.prioridad - b.prioridad);    
    return html`
      <center><h1>${this.title}</h1></center>
      <to-do-input @agregar-tarea="${this.agregar}"></to-do-input>
      <ul>
      ${ordenarTarea.map(
        function(tarea)
        {
          return html `
            <to-do-task id="${tarea.id}" tarea="${tarea.texto}" prioridad="${tarea.prioridad}" @remover-tarea="${this.eliminar}"></to-do-task>
          `;
        }.bind(this)
      )}
      </ul>
  `;
  }

  agregar(e){
    var tarea={"id":this.getId(),"texto":e.detail.texto,"prioridad":e.detail.prioridad};
    this.tareas=[...this.tareas,tarea];
  }

  getId(){
    return Date.now();    
  }

  eliminar(e){
    this.tareas=this.tareas.filter(function(tarea)
    {
      return tarea.id != e.detail.id;
    });
  }

}  
customElements.define('to-do-app', ToDoApp) 