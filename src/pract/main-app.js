import { LitElement, html, css } from 'lit-element';
import "./to-do-app"
import "../api/test-api"
import "../api/img-api"
class MainApp extends LitElement {
  static get properties(){
    return{
    };
  } 
  constructor(){
    super();
    setInterval(this.createImg.bind(this),5000);
    
  }
  static get styles(){
    return css`
      div{
        text-align:center;
      }
    `
  }
  render() { 
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class='main'>
        <p class="p-2 bg-primary text-white">Lista de tareas</p>
        <to-do-app tareas='
          [
            {"id":"1622669693088","texto":"Tres","prioridad":"3"},
            {"id":"1622669693080","texto":"Uno","prioridad":"1"},
            {"id":"1622669693089","texto":"Dos","prioridad":"2"}
          ]
          '></to-do-app>
      </div>
      <img-api></img-api>
      <div id="img"></div>
      <test-api></test-api>
  `;
  }

  createImg(){
    var currentDiv = document.getElementById("img");
    var newImg = document.createElement("img-api");
    document.body.insertBefore(newImg, currentDiv);
  }

}  
customElements.define('main-app', MainApp) 