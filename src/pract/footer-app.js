import { LitElement, html, css } from 'lit-element';
class FooterApp extends LitElement {
  static get properties(){
    return{

    };
  } 
  constructor(){
    super();
  }
  static get styles(){
    return css`
      .footer{
        background-color: black;
        position: fixed;
        bottom: 0px;
        width: 100%;
        height: 40px;
        color: white;
        text-align: right;
      }
    `
  }
  render() { 
    return html`
      <div class='footer'><h2>CM</h2></div>
  `;
  } 
}  
customElements.define('footer-app', FooterApp) 